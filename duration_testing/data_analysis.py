# -*- coding: utf-8 -*-
"""
Created on Fri Sep 15 10:22:35 2023

@author: mrehberg
"""

import pandas as pd
import os
import datetime


def parse_data():
    data = pd.read_excel(f'{cwd}\simplified_log.xlsx')
    
    # fill day and date from above
    data['date'] = data['date'].ffill()
    data['day'] = data['day'].ffill()
    
    #add day to time
    data['time_total'] = data['day'] + data['Time_int']    
    
    # remove data without a rover number
    data = data.dropna(subset=['Rover'])
    
    # dissolve for each rover
    rv1 = data[data['Rover']==1].reset_index(drop=True)
    rv2 = data[data['Rover']==2].reset_index(drop=True)
    
       
    return data, rv1, rv2


def q1(x):
    return x.quantile(0.25)

def q2(x):
    return x.quantile(0.5)

def q3(x):
    return x.quantile(0.75)


def event_parse(rv):
    
    # fill energy for rover
    rv['Energy (amp-h)'] = rv['Energy (amp-h)'].ffill()
    rv['Delivered mass (kg)'] = rv['Delivered mass (kg)'].ffill()
    
    # create events for each row
    for row in range(len(rv)-1):
        rv.loc[row, 'full_event'] = str(rv['Event'][row]) + '_to_' + str(rv['Event'][row+1])
        rv.loc[row, 'event_time_minutes'] = (rv['time_total'][row+1] - rv['time_total'][row])*24*60
       # rv.loc[row, 'energy_usage'] = rv['Energy (amp-h)'][row+1] - rv['Energy (amp-h)'][row]
        #rv.loc[row, 'mass_deliv'] = rv['Delivered mass (kg)'][row+1] - rv['Delivered mass (kg)'][row]
        
    #rv['mass_deliv'] = rv['mass_deliv'].fillna(0)
    rv['repair'] = rv['full_event'].str.contains(r'repair.*_to.*repair')
    rv = rv[rv['repair']==True]
    return rv

def group_data(rv1_ev, rv2_ev):
    gb_data = pd.concat([rv1_ev, rv2_ev])
    keep = ['full_event', 'event_time_minutes', 'energy_usage', 'mass_deliv']
    gb = gb_data[keep].groupby('full_event').agg({'event_time_minutes':[q1, q2, q3, 'sum', 'count']})
    return gb


if __name__ =='__main__':
    cwd = os.path.dirname(os.path.realpath(__file__))
    data, rv1, rv2 = parse_data()
    rv1_ev = event_parse(rv1)
    rv2_ev = event_parse(rv2)
    total = pd.concat([rv1_ev, rv2_ev])
    total.to_csv('repairs.csv')
    #gb = group_data(rv1_ev, rv2_ev)
    