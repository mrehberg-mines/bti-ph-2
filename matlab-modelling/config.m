%% Rover 1 
rv=rover;

rv.basePower = 250; %W
rv.breakingRate = 210/3600*0.85; %kg/s
rv.breakingPower = 1800/0.88 + rv.basePower; %W
rv.loadCapacity = 80; % kg
rv.loadingRate = 160/3600; %kg/s
rv.loadingPower = 200 + rv.basePower; %W
rv.unloadingRate = 600/3600; %kg/s
rv.baseSpeed = 2; %m/s
rv.movingPower = 1600; %W
rv.batterySize = 13800*3600; %watt-s
rv.chargingPower = -4800; %W 3450

rv.baseMass = 400; %kg
rv.breakingSpeed = 0.01; %m/s
rv.speedCoef= 0; % (m/s)/kg
rv.chargingDistance = 3; %m
rv.chargingDelay = 120; %sec
rv.depthOfDischarge = 0.25; % percent
rv.initialCharge = 0; % Does nothing
rv.acceleration = 0.05; % m/s2
rv.powerCoef = 0; %W/kg


%% Rover 2 
rv2=rover;

rv2.breakingRate = rv.breakingRate; %kg/s
rv2.breakingPower = rv.breakingPower; %W
rv2.breakingSpeed = rv.breakingSpeed; %m/s
rv2.loadCapacity = rv.loadCapacity; % kg

rv2.loadingRate = rv.loadingRate; %kg/s
rv2.loadingPower = rv.loadingPower; %W

rv2.unloadingRate = rv.unloadingRate; %kg/s

rv2.speedCoef= rv.speedCoef; % (m/s)/kg
rv2.baseSpeed = rv.baseSpeed ; %m/s
rv2.movingPower = rv.movingPower; %W
rv2.acceleration = rv.acceleration; % m/s2
rv2.powerCoef = rv.powerCoef; %W/kg

rv2.baseMass = rv.baseMass ; %kg
rv2.batterySize = rv.batterySize; %watt-s
rv2.chargingPower = rv.chargingPower; %W
rv2.chargingDistance = rv.chargingDistance; %m
rv2.chargingDelay = rv.chargingDelay; %sec
rv2.depthOfDischarge = 0.25; % percent

rv2.initialCharge = 1; %to offset charging load


%% Rover buse elements
rvelem(1)=Simulink.BusElement;
rvelem(1).Name = 'batterySize';
rvelem(1).DataType = "double";

rvelem(2)=Simulink.BusElement;
rvelem(2).Name = 'chargingDistance';
rvelem(2).DataType = "double";

rvelem(3)=Simulink.BusElement;
rvelem(3).Name = 'loadCapacity';
rvelem(3).DataType = "double";

rvelem(4)=Simulink.BusElement;
rvelem(4).Name = 'chargingDelay';
rvelem(4).DataType = "double";

rvelem(5)=Simulink.BusElement;
rvelem(5).Name = 'depthOfDischarge';
rvelem(5).DataType = "double";

rvelem(6)=Simulink.BusElement;
rvelem(6).Name = 'initialCharge';
rvelem(6).DataType = "double";


bus_rv = Simulink.Bus;
bus_rv.Elements = rvelem;
%% State bus elements

elem(1)=Simulink.BusElement;
elem(1).Name = 'Power';
elem(1).DataType = "double";

elem(2)=Simulink.BusElement;
elem(2).Name = 'Location';
elem(2).DataType = "double";

elem(3)=Simulink.BusElement;
elem(3).Name = 'Loaded_Mass';
elem(3).DataType = "double";

elem(4)=Simulink.BusElement;
elem(4).Name = 'Excavated_Mass';
elem(4).DataType = "double";

elem(5)=Simulink.BusElement;
elem(5).Name = 'State';
elem(5).DataType = "int8";

elem(6)=Simulink.BusElement;
elem(6).Name = 'gotoState';
elem(6).DataType = "int8";

curState = Simulink.Bus;
curState.Elements = elem;

%% Simulation
out=sim("rvSim");
out2=sim("rv2Sim");


%% Plots
close all
[int_modes, name_modes]=enumeration('modes');
subplot1=subplot(3,1,1);
% set time to x
x=out.simout.var.Power.Time./3600;
rv_color=[0 0.4470 0.7410];
rv2_color=[0.6350 0.0780 0.1840];

% Location 
y1_1 = out.simout.var.Location.Data;
y1_2 = out2.simout.var.Location.Data;
plot(x,y1_1)
hold on
plot(x,y1_2)


% Battery
subplot2=subplot(3,1,2); 
y2_1 = out.battery./rv.batterySize;
y2_2 = out2.battery./rv.batterySize;
plot(x,y2_1)
hold on 
plot(x, y2_2)
hold off

% Mode
subplot3=subplot(3,1,3); 
y3_1 = out.simout.gotoState.Data;
y3_2 = out2.simout.gotoState.Data;
plot(x,y3_1)
hold on
plot(x, y3_2)
hold off

ylabel(subplot1, 'Location (m)');
ylabel(subplot2, 'Battery %');
ylabel(subplot3, 'ConOp Mode'); 
set(subplot3,'YTickLabel',name_modes);
xlabel(subplot3, 'Time (hr)');
% 
% 
averagePower=round(sum((out.simout.var.Power.Data>=0).*out.simout.var.Power.Data)/...
    sum(out.simout.var.Power.Data>=0),1);
% 
% dailyPower=sum((out.simout.var.Power.Data>=0).*(out.simout.var.Power.Data))/3600/1000;%kWh
% totalPower=dailyPower*15*2; %kWh
% 
idleTime=round(sum(out.simout.gotoState.Data==int8(modes.idle))/3600,1);
loadTime=round(sum(out.simout.gotoState.Data==int8(modes.loading))/3600,1);
breakTime=round(sum(out.simout.gotoState.Data==int8(modes.breaking))/3600,1);
travelTime=round(sum(out.simout.gotoState.Data==int8(modes.travelling))/3600,1);
chargeTime=round(sum(out.simout.gotoState.Data==int8(modes.charging))/3600,1);
disp(strcat("kg Delivered: ",string(round(max(out.total),1)), " kg"));
disp(strcat("Average Power : ", string(averagePower), " W"));
disp(strcat("Hours Charging: ", string(chargeTime), " hr"));
disp(strcat("Hours Breaking: ", string(breakTime), " hr"));
disp(strcat("Hours Loading: ", string(loadTime), " hr"));
disp(strcat("Hours Travelling: ", string(travelTime), " hr"));
disp(strcat("Hours Idle: ",string(idleTime), " hr"));